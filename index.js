console.log("Hello World");

let firstName = "John";
console.log(firstName);

let lastName = "Smith";
console.log(lastName);

let age = 30;
console.log(age);

let hobbies = ["Biking", "Mountain Climbing", "Swimming"];
console.log(hobbies);

let workAddress = {
	houseNumber: "32",
	street: "Washington",
	city: "Lincoln",
	state: "Nebraska"
}
console.log(workAddress);

let fullName = "Steve Rogers";
	console.log("My full name is " + fullName);

	let currentAge = 40;
	console.log("My current age is: " + currentAge);
	
	let friends = ["Tony","Bruce", "Thor", "Natasha", "Clint", "Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	}
	console.log("My Full Profile: ")
	console.log(profile);

	fullName = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName);

	const lastLocation = "Arctic Ocean";
	console.log("I was found frozen in: " + lastLocation);